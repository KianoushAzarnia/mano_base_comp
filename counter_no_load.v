module counter_no_load #(parameter LEN)(input clk, rst, clear, inc, output cout, output [LEN-1:0] out);

wire load = 0;
wire[LEN-1:0] in = 0;

counter#(LEN) counter_unit (clk, rst, clear, load, inc, in, cout, out);

endmodule
