module register#(parameter LEN)
    (input clk, rst, clear, load, input[LEN-1:0] in, output reg[LEN-1:0] out);

always@(posedge clk, rst) begin
    if(rst)
        out <= 0;
    else begin
        if(clear) out <= 0;
        else if(load) out <= in;
        else out <= out;
    end
end

endmodule