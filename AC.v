module AC #(parameter LEN)
    (input[LEN-1:0] in, input rst, CLR, LD, INR, clk, output[LEN-1:0] out, output E);

    counter#(LEN) AC_reg 
        (.clk(clk), .rst(rst), .clear(CLR), .load(LD), .inc(INR), .in(in), .cout(E), .out(out));

endmodule