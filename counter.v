module counter #(parameter LEN)(input clk, rst, clear, load, inc, input[LEN-1:0] in, output reg cout, output reg[LEN-1:0] out);

always@(posedge clk, rst) begin
    if(rst) begin
        out <= 0;
        cout <= 0; 
    end
    else begin
        if(clear) {cout ,out} <= 0;
        else if(load) {cout, out} <= in;
        else if(inc) {cout, out} <= out + 1;
        else out <= out;
    end
end

endmodule
