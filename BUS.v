module BUS #(parameter LEN)(
    input[2:0] select,
    input[LEN-1:0]
        mem_out,
        ar_out,
        pc_out,
        dr_out,
        ac_out,
        ir_out,
        tr_out,
    output reg [LEN-1:0] out);

localparam
    AR = 1,
    PC = 2,
    DR = 3,
    AC = 4,
    IR = 5,
    TR = 6,
    MEM = 7;
    
always@(*) begin
case(select)
    AR: out = ar_out;
    PC: out = pc_out;
    DR: out = dr_out;
    AC: out = ac_out;
    IR: out = ir_out;
    TR: out = tr_out;
    MEM: out = mem_out;
endcase
end

endmodule

