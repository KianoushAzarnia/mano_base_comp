module ALU #(parameter LEN)
    (input[LEN-1:0] a, b, input[3:0] select, output reg[LEN-1:0] result, output cout, equl);

wire [1:0] ins_type, logic_type;
wire [2:0] arith_type;

reg temp;

assign ins_type = select[3:2];
assign logic_type = select[1:0];
assign arith_type = select[1:0];

localparam 
    ARITHMATIC =  2'b00,
    LOGICAL =     2'b01,
    CIR_SHIFT_R = 2'b10,
    CIR_SHIFT_L =  2'b11;

localparam
    TRANSFER =  2'b00,
    INCREMENT = 2'b01,
    ALU_ADD =   2'b10,
    ALU_SUB =   2'b11;

localparam
    LOG_AND = 2'b00,
    LOG_OR = 2'b01,
    LOG_XOR = 2'b10,
    LOG_COMP = 2'b11;

always@(*) begin
    case(ins_type)
        ARITHMATIC: 
        case(arith_type)
            TRANSFER: result = a;
            INCREMENT: result = a + 1;
            ALU_ADD: result = a + b;
            ALU_SUB: result = a - b;
        endcase

        LOGICAL: 
        case(logic_type)
            LOG_AND: result = a & b; 
            LOG_OR: result = a | b; 
            LOG_XOR: result = a ^ b; 
            LOG_COMP: result = ~b;    
        endcase
        CIR_SHIFT_R: begin
            temp = b[0];
            result = {1'b0, b[LEN-1:1]};
            result[LEN-1] = temp;
        end 
        CIR_SHIFT_L: begin
            temp = b[0];
            result = {b[LEN-2:0], 1'b0};
            result[0] = temp;
        end
    endcase
end

endmodule