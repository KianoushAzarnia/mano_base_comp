`timescale 1ns/1ns
module testbench();

    reg clk, rst;  
    wire[7:0] external_data;

    mano_computer mycomp(.clk(clk), .rst(rst),.external_data(external_data));

    //reg write_enable, read_enable;  reg[15:0] value;  
    //reg[11:0] address, address_external;  wire[15:0] out, out_external;
    
    //memory #(.LEN(16), .MLOG(12))mymem(clk, write_enable, read_enable, value,
    //address, address_external, out, out_external);


    
    initial begin
        clk = 0;
        rst = 1;
        //#5  address_external = 2;
        #2  rst = 0;
        //#5  address = 2;
        //#5  value = 777;
        //#5  write_enable = 1;
        //#5  read_enable = 1;
    end

    initial repeat(200) begin
        #1 clk = ~clk;
    end
endmodule