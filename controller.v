module controller(
    input clk, rst, dr0, input[3:0] opcode, 
    output reg 
    clr_ar, ld_ar, inr_ar,
    clr_pc, ld_pc, inr_pc,
    clr_dr, ld_dr, inr_dr,
    clr_ac, ld_ac, inr_ac,
    clr_tr, ld_tr, inr_tr,
    clr_ir, ld_ir, inr_ir,
    mem_write, mem_read,
    output reg[2:0] select_bus, output reg[3:0] select_alu 
);

localparam
    AND =   4'b0001,
    ADD =   4'b0010,
    LDA =   4'b0011,
    STA =   4'b0100,
    BUN =   4'b0101,
    BSA =   4'b0110,
    ISZ =   4'b0111,
    CMA =   4'b1000,
    OR =    4'b1001,
    CIR =   4'b1010,
    CIL =   4'b1011,
    HALT =  4'b1100;
    //NotDirect = 4'b1101;



localparam
    idle = 1,
    fetch1 = 2,
    fetch2 = 3,
    decode = 4,
    add1 = 5,
    add2 = 6,
    and1 = 7,
    and2 = 8,
    halt = 9,
    add3 = 10,
    dummy = 11,
    sta1 = 12,
    sta2 = 13,
    and3 = 14,
    lda1 = 15,
    lda2 = 16,
    lda3 = 17,
    bun1 = 18,
    bun2 = 19,
    bsa1 = 20,
    bsa2 = 21,
    bsa3 = 22,
    funny = 23,
    cma1 = 24,
    or1 = 25,
    or2 = 26,
    or3 = 27,
    cir_L1 = 28,
    cir_R1 = 29,
    isz1 = 30,
    isz2 = 31,
    isz3 = 32,
    isz4 = 33;
    //notDirect2 = 24;

localparam 
    ARITHMATIC =  2'b00,
    LOGICAL =     2'b01,
    CIR_SHIFT_R = 2'b10,
    CIR_SHIFT_L =  2'b11;

localparam
    TRANSFER =  2'b00,
    INCREMENT = 2'b01,
    ALU_ADD =   2'b10,
    ALU_SUB =   2'b11;

localparam
    LOG_AND = 2'b00,
    LOG_OR = 2'b01,
    LOG_XOR = 2'b10,
    LOG_COMP = 2'b11;

reg[4:0] ps, ns;

always@(*) begin: what_is_next_state
    if(rst)
      ns = idle;
    else case(ps)
        halt: ns = halt;
        idle: ns = fetch1;
        fetch1: ns = fetch2;
        fetch2: ns = decode;
        decode:
            case(opcode)
                ADD: ns = add1;
                STA: ns = sta1;
                AND: ns = and1;
                LDA: ns = lda1;
                BUN: ns = bun1;
                BSA: ns = bsa1;
                //NotDirect: ns = notDirect1;
                CMA: ns = cma1;
                OR: ns = or1;
                CIL: ns = cir_L1;
                CIR: ns = cir_R1;
                ISZ: ns = isz1;
                HALT: ns = halt;
                default: ns = fetch1;
            endcase
        add1: ns = add2;
        add2: ns = add3;
        add3: ns = dummy;
        sta1: ns = sta2;
        sta2: ns = dummy;
        and1: ns = and2;
        and2: ns = and3;
        and3: ns = dummy;
        lda1: ns = lda2;
        lda2: ns = lda3;
        lda3: ns = dummy;
        bun1: ns = bun2;
        bun2: ns = dummy;
        bsa1: ns = bsa2;
        bsa2: ns = bsa3;
        bsa3: ns = dummy;
        //notDirect1: ns = notDirect2;
        //notDirect2: ns = dummy;
        cma1: ns = dummy;
        or1: ns = or2;
        or2: ns = or3;
        or3: ns = dummy;
        cir_L1: ns = dummy;
        cir_R1: ns = dummy;
        isz1: ns = isz2;
        isz2: ns = isz3;
        isz3: ns = isz4;
        isz4: ns = dummy;
        dummy: ns = fetch1;
        default: ns = fetch1;
    endcase
end

localparam
    AR = 1,
    PC = 2,
    DR = 3,
    AC = 4,
    IR = 5,
    TR = 6,
    MEM = 7;

always@(*) begin : outout_signals
    {clr_ar, ld_ar, inr_ar,
    clr_pc, ld_pc, inr_pc,
    clr_dr, ld_dr, inr_dr,
    clr_ac, ld_ac, inr_ac,
    clr_tr, ld_tr, inr_tr,
    clr_ir, ld_ir, inr_ir,
    mem_write, mem_read, 
    select_bus, select_alu} = 0;
    
    case(ps)
        idle: 
            clr_pc = 1;
        fetch1: begin
            select_bus = PC;
            ld_ar = 1;
            inr_pc = 1;
        end
        fetch2: begin
            mem_read = 1;
            select_bus = MEM;
            ld_ir = 1;
        end
        decode:
            ;//do nothing        
        add1: begin
            select_bus = IR;
            ld_ar = 1;
        end
        add2: begin
            mem_read = 1;
            select_bus = MEM;
            ld_dr = 1;
        end
        add3: begin
            // sure dr_out value is valid
            select_alu = {ARITHMATIC, ALU_ADD};
            ld_ac = 1;
        end
        sta1: begin
            select_bus = IR;
            ld_ar = 1;  
        end
        sta2: begin
            select_bus = AC;
            mem_write = 1; 
        end
        and1: begin
            select_bus = IR;
            ld_ar = 1;  
        end
        and2: begin
            mem_read = 1;
            select_bus = MEM;
            ld_dr = 1;
        end
        and3: begin
            // sure dr_out value is valid
            select_alu = {LOGICAL, LOG_AND};
            ld_ac = 1;
        end
        lda1: begin
            select_bus = IR;
            ld_ar = 1;  
        end
        lda2: begin
            mem_read = 1;
            select_bus = MEM;
            ld_dr = 1;
        end
        lda3: begin
            // sure dr_out value is valid
            select_alu = {ARITHMATIC, TRANSFER};
            ld_ac = 1;
        end
        bun1: begin
            select_bus = IR;
            ld_ar = 1; 
        end
        bun2: begin
            select_bus = AR;
            ld_pc = 1;  
        end
        bsa1: begin
            select_bus = IR;
            ld_ar = 1; 
        end
        bsa2: begin
            select_bus = PC;
            mem_write = 1;
            inr_ar = 1;
        end
        bsa3: begin
            select_bus = AR;
            ld_pc = 1;
        end
        cma1: begin
            // sure dr_out value is valid
            select_alu = {LOGICAL, LOG_COMP};
            ld_ac = 1;
        end
        cir_L1: begin
            select_alu = {CIR_SHIFT_L, 2'b00};
            ld_ac = 1;
        end
        cir_R1: begin
            select_alu = {CIR_SHIFT_R, 2'b00};
            ld_ac = 1;
        end
        or1: begin
            select_bus = IR;
            ld_ar = 1;  
        end
        or2: begin
            mem_read = 1;
            select_bus = MEM;
            ld_dr = 1;
        end
        or3: begin
            // sure dr_out value is valid
            select_alu = {LOGICAL, LOG_OR};
            ld_ac = 1;
        end
        isz1: begin
            select_bus = IR;
            ld_ar = 1;
        end
        isz2: begin
            mem_read = 1;
            select_bus = MEM;
            ld_dr = 1;
            
        end
        isz3: begin
            inr_dr = 1;
            select_bus = DR;
        end
        isz4: begin    
            
            mem_write = 1;
            inr_pc = (dr0 == 1) ? 1 : 0;
        end
        halt:
            ; //do nothing
        funny:
            inr_pc = 1;
        /*bsa3: begin 
            inr_ar = 1;
            select_bus = AR;
            ld_pc = 1;
        end
        bsa4: begin 
            mem_read = 1;
            select_bus = MEM;
            ld_ar = 1;
        end*/
        /*notDirect1: begin
            select_bus = IR;
            ld_ar = 1; 
        end
        notDirect2: begin
            mem_read = 1;
            select_bus = MEM;
            ld_ar = 1;
        end*/ 
        default:
        {clr_ar, ld_ar, inr_ar,
        clr_pc, ld_pc, inr_pc,
        clr_dr, ld_dr, inr_dr,
        clr_ac, ld_ac, inr_ac,
        clr_tr, ld_tr, inr_tr,
        clr_ir, ld_ir, inr_ir,
        mem_write, mem_read, 
        select_bus, select_alu} = 0;
    endcase
end

always@(posedge clk, rst) begin: go_next
    if(rst)
        ps <= idle;
    else
        ps <= ns;
end

endmodule