module datapath #(parameter LEN = 16, parameter MLOG = 12, parameter FPGA_IN = 8) (
    input clk, rst,
    input clr_ar, ld_ar, inr_ar,
    input clr_pc, ld_pc, inr_pc,
    input clr_dr, ld_dr, inr_dr,
    input clr_ac, ld_ac, inr_ac,
    input clr_tr, ld_tr, inr_tr,
    input clr_ir, ld_ir, inr_ir,
    input mem_write, mem_read,
    input[2:0] select_bus, input[3:0] select_alu, 
    output E, output[3:0] opcode , output[FPGA_IN-1:0] out_external, output dr0);

    wire [LEN-1:0] bus_out, dr_out, ac_out, tr_out, memory_out, ir_out, alu_out;
    wire [MLOG-1:0] ar_out, pc_out;
    
    assign opcode = ir_out[LEN-1:LEN-4];
    assign out_external = ac_out[FPGA_IN-1:0];
    wire dummy;
    counter #(MLOG) ar(.clk(clk), .rst(rst), .clear(clr_ar), .load(ld_ar), .inc(inr_ar), .in(bus_out[MLOG-1:0]), .cout(dummy), .out(ar_out));
    counter #(MLOG) pc(.clk(clk), .rst(rst), .clear(clr_pc), .load(ld_pc), .inc(inr_pc), .in(bus_out[MLOG-1:0]), .cout(dummy), .out(pc_out));
    counter #(LEN) dr(.clk(clk), .rst(rst), .clear(clr_dr), .load(ld_dr), .inc(inr_dr), .in(bus_out), .cout(dummy), .out(dr_out));
    counter #(LEN) ac(.clk(clk), .rst(rst), .clear(clr_ac), .load(ld_ac), .inc(inr_ac), .in(alu_out), .cout(E), .out(ac_out));
    counter #(LEN) tr(.clk(clk), .rst(rst), .clear(clr_tr), .load(ld_tr), .inc(inr_tr), .in(bus_out), .cout(dummy), .out(tr_out));
    counter #(LEN) ir(.clk(clk), .rst(rst), .clear(clr_ir), .load(ld_ir), .inc(inr_ir), .in(bus_out), .cout(dummy), .out(ir_out));
    BUS #(LEN) bus_unit (.select(select_bus), .mem_out(memory_out), .ar_out({4'b0000,ar_out}), .pc_out({4'b0000,pc_out}), .dr_out(dr_out), .ac_out(ac_out), .ir_out(ir_out), .tr_out(tr_out), .out(bus_out));
    memory #(LEN, MLOG) mem_unit (.clk(clk), .write_enable(mem_write), .read_enable(mem_read), .value(bus_out), .address(ar_out), .out(memory_out));
    ALU #(LEN) alu_unit (.a(dr_out), .b(ac_out), .select(select_alu), .result(alu_out), .cout(), .equl());
    
    assign dr0 = ~(|dr_out);
    
endmodule