module memory #(parameter LEN, parameter MLOG)(
    input clk, write_enable, read_enable, 
    input [LEN-1:0] value,
    input [MLOG-1:0] address, output reg[LEN-1:0] out);

    integer i;
    reg[LEN-1:0] memory_vector[(2**LEN) - 1: 0];

	initial begin
	// for (i=0; i<2**LEN; i=i+1) memory_vector[i] = {(LEN-1){1'b0}};							
	//$readmemh("program.txt",memory_vector);
    
    //a program that adds AC with 3 twice, result is 6
    memory_vector[0] = 16'b0010000000010100;
    memory_vector[1] = 16'b0010000000010100;
    //then stores the result in 100th line
    memory_vector[2] = 16'b0100000001100100;
    //and memory[20] with AC result is 2
    memory_vector[3] = 16'b0001000000010100;
    //load memory[21] result is 7
    memory_vector[4] = 16'b0011000000010101;
    
    memory_vector[5] = 16'b0101000000000111; // Branch Unconditional to memory[7] PC <- AR
    //and memory[22] with ac this must not execute and ac_out(result) will not be zero:
    memory_vector[6] = 16'b0001000000010110;
    memory_vector[7] = 16'b0010000000010100; //add result is 10;
    //Branch and save address to memory[11]:
    memory_vector[8] = 16'b0110000000001011;
    memory_vector[9] = 16'b0010000000010100; //this won't be execute
    memory_vector[10] = 16'b0101000000001011; //Branch Unconditional to memory[11]
    //and memory[22] with ac this must not execute and ac_out(result) will not be zero:
    memory_vector[11] = 16'b0001000000010110;
    memory_vector[12] = 16'b0010000000010101; //before above adds with 7 result is 17 
    //memory_vector[13] = 16'b0001000000010110; //and with memory[22] result is 0
    //memory_vector[14] = 16'b1000000000010110; //Complement AC
    //memory_vector[13] = 16'b1001000000010111; //or with memory[23] result is 65535;
    memory_vector[13] = 16'b0011000000010111; //load mem[23] to AC
    memory_vector[14] = 16'b1010000000010111; //CIR Shift R AC result is 1;
    memory_vector[15] = 16'b0111000000011000; //ISZ mem[24]
    memory_vector[16] = 16'b1100000000000000; //halt
    memory_vector[20] = 3;
    memory_vector[21] = 7;
    memory_vector[22] = 0;
    memory_vector[23] = 16'b0000000000000010;
    memory_vector[24] = -1;
    
	end

always@(*) begin
    if(read_enable)
        out <= memory_vector[address];
end

always@(posedge clk) begin
    if(write_enable)
        memory_vector[address] <= value;
end

endmodule