module mano_computer #(parameter LEN = 16, FPGA_IN = 8, MLOG = 12)
    (input clk, rst, output[FPGA_IN-1:0] external_data);

    wire clr_ar; wire ld_ar; wire inr_ar;
    wire clr_pc; wire ld_pc; wire inr_pc;
    wire clr_dr; wire ld_dr; wire inr_dr;
    wire clr_ac; wire ld_ac; wire inr_ac;
    wire clr_tr; wire ld_tr; wire inr_tr;
    wire clr_ir; wire ld_ir; wire inr_ir;
    wire mem_write; wire mem_read;
    wire dr0;
    wire[2:0] select_bus;
    wire[3:0] select_alu; 
    wire E; wire[3:0] opcode;

    datapath mydatapath(.clk(clk), .rst(rst),
        .clr_ar(clr_ar), .ld_ar(ld_ar), .inr_ar(inr_ar),
        .clr_pc(clr_pc), .ld_pc(ld_pc), .inr_pc(inr_pc),
        .clr_dr(clr_dr), .ld_dr(ld_dr), .inr_dr(inr_dr),
        .clr_ac(clr_ac), .ld_ac(ld_ac), .inr_ac(inr_ac),
        .clr_tr(clr_tr), .ld_tr(ld_tr), .inr_tr(inr_tr),
        .clr_ir(clr_ir), .ld_ir(ld_ir), .inr_ir(inr_ir),
        .mem_write(mem_write), .mem_read(mem_read),
        .select_bus(select_bus), .select_alu(select_alu), 
        .E(E), .opcode(opcode), .out_external(external_data), .dr0(dr0));

    controller mycontroller(
    .clk(clk), .rst(rst),  .dr0(dr0), .opcode(opcode),  
    .clr_ar(clr_ar), .ld_ar(ld_ar), .inr_ar(inr_ar),
    .clr_pc(clr_pc), .ld_pc(ld_pc), .inr_pc(inr_pc),
    .clr_dr(clr_dr), .ld_dr(ld_dr), .inr_dr(inr_dr),
    .clr_ac(clr_ac), .ld_ac(ld_ac), .inr_ac(inr_ac),
    .clr_tr(clr_tr), .ld_tr(ld_tr), .inr_tr(inr_tr),
    .clr_ir(clr_ir), .ld_ir(ld_ir), .inr_ir(inr_ir),
    .mem_write(mem_write), .mem_read(mem_read),
    .select_bus(select_bus), .select_alu(select_alu));
endmodule